package Chat.Client;

import Chat.Network.TCPConnection;
import Chat.Network.TCPConnectionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ClientWindow extends JFrame implements ActionListener, TCPConnectionListener {

    final String IP = "100.71.194.7";
    final int port = 8189;
    private final static int HEIGHT = 500;
    private final static int WEIGHT = 400;

    private TCPConnection tcpConnection;

    JTextArea textArea = new JTextArea();
    JTextField loginField = new JTextField();
    JTextField messageField = new JTextField();


    public ClientWindow(User user) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WEIGHT, HEIGHT);
        setAlwaysOnTop(true);


        textArea.setEditable(false);
        textArea.setLineWrap(true);
        loginField.setText(user.getLogin());
        loginField.setEditable(false);
        add(textArea, BorderLayout.CENTER);
        add(loginField, BorderLayout.NORTH);
        add(messageField, BorderLayout.SOUTH);


        messageField.addActionListener(this);
        setVisible(true);
        try {
            tcpConnection = new TCPConnection(this, IP, port, user);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String msg = messageField.getText();
        if (msg.equalsIgnoreCase("")) return;
        messageField.setText(null);
        tcpConnection.sendMessage(loginField.getText() + " : " + msg);

    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMessage(tcpConnection.getUser().getLogin() + " connected");
        printMessage(tcpConnection+"");

    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        printMessage(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMessage("Connection closed");
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception ex) {
        printMessage("" + ex);
    }

    private synchronized void printMessage(final String value) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                textArea.append(value + "\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
            }
        });
    }

}
