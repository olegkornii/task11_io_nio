package Chat.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginWindow extends JFrame implements ActionListener {

    private final int WIDTH = 200;
    private final int HEIGHT = 100;
    @Override
    public void actionPerformed(ActionEvent e) {

    }
    JTextArea question = new JTextArea("What is you name?");
    JTextField answer = new JTextField();
    JButton button = new JButton("Continue");

    public LoginWindow(){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH,HEIGHT);
        setAlwaysOnTop(true);

        add(question,BorderLayout.NORTH);
        add(answer, BorderLayout.CENTER);
        add(button, BorderLayout.SOUTH);

        question.setEditable(false);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String login = answer.getText();
                setVisible(false);
                User user = new User(login);
                new ClientWindow(user);
            }
        });
        setVisible(true);

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LoginWindow();
            }
        });
    }

}
