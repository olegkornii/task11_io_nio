package Chat.Client;

import Chat.Network.TCPConnection;

import java.util.ArrayList;
import java.util.List;

public class UserBase {
    private static List<TCPConnection> userList = new ArrayList<>();
    void addUser(TCPConnection tcpConnection){
        userList.add(tcpConnection);
    }

    public static List<TCPConnection> getUserList() {
        return userList;
    }
}
