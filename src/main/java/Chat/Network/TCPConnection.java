package Chat.Network;

import Chat.Client.User;

import java.io.*;
import java.net.Socket;


public class TCPConnection {

    private final Socket socket;
    private final Thread thread;
    private final TCPConnectionListener listener;
    private final BufferedReader in;
    private final BufferedWriter out;
    private final User user;

    public TCPConnection(TCPConnectionListener tcpConnectionListener, String ipAddress, int port, User user) throws IOException {

        this(new Socket(ipAddress, port), tcpConnectionListener, user);

    }

    public TCPConnection(final Socket socket, final TCPConnectionListener listener, User user) throws IOException {
        this.socket = socket;
        this.listener = listener;
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.user = user;
        this.thread = new Thread(new Runnable() {
            public void run() {
                try {
                    listener.onConnectionReady(TCPConnection.this);
                    while (!thread.isInterrupted()) {
                        String msg = in.readLine();
                        listener.onReceiveString(TCPConnection.this, msg);
                    }

                } catch (IOException e) {
                    listener.onException(TCPConnection.this, e);
                    System.out.println("line 37 Tcpc");
                } finally {
                    listener.onDisconnect(TCPConnection.this);
                }
            }
        });
        this.thread.start();
    }

    public synchronized void sendMessage(String value) {

        try {
            out.write(value + "\r\n");
            out.flush();
        } catch (IOException e) {
            listener.onException(TCPConnection.this, e);
            System.out.println("Line 54");
            disconnect();
        }
    }

    public synchronized void disconnect() {
        thread.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            listener.onException(TCPConnection.this, e);
            System.out.println("Line 63 TCPC");
        }
    }

    @Override
    public String toString() {
        return "TCPConnection : " + socket.getInetAddress() + " : " + socket.getPort();
    }

    public User getUser() {
        return user;
    }

    public synchronized String getMessage() throws IOException {
        return in.readLine();
    }
}
