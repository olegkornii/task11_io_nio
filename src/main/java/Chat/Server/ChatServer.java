package Chat.Server;

import Chat.Client.User;
import Chat.Client.UserBase;
import Chat.Network.TCPConnection;
import Chat.Network.TCPConnectionListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;


public class ChatServer implements TCPConnectionListener {
    private List<TCPConnection> connectionList = new ArrayList<>();
    private UserBase userBase= new UserBase();
    public static void main(String[] args) {
        new ChatServer();
    }

    private ChatServer() {

        System.out.println("Server running");
        try (ServerSocket serverSocket = new ServerSocket(8189)) {
            System.out.println(serverSocket.getInetAddress());
            System.out.println(serverSocket.getLocalSocketAddress());

            while (true) {
                try {
                    System.out.println(connectionList);
                    new TCPConnection(serverSocket.accept(), this, new User());

                    System.out.println(serverSocket.getInetAddress());
                    System.out.println(serverSocket.getLocalSocketAddress());

                } catch (Exception e) {
                    System.out.println("Something goes wrong((");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void onConnectionReady(TCPConnection tcpConnection) {

        connectionList.add(tcpConnection);
        sendToAllConnections(tcpConnection + " connected");

    }

    @Override
    public synchronized void onReceiveString(TCPConnection tcpConnection, String value) {
        sendToAllConnections( value);
    }

    @Override
    public synchronized void onDisconnect(TCPConnection tcpConnection) {
        connectionList.remove(tcpConnection);

    }

    @Override
    public synchronized void onException(TCPConnection tcpConnection, Exception ex) {
        System.out.println("TCP connection exception : " + ex);
    }

    private void sendToAllConnections(String value) {
        System.out.println(value);
        for (TCPConnection tcpCL :
                connectionList) {
            tcpCL.sendMessage(value);
        }

    }
    private void registration(TCPConnection tcpConnection){
        tcpConnection.sendMessage("What is your name?");
        try {
            String login = tcpConnection.getMessage();
            tcpConnection.getUser().setLogin(login);
            tcpConnection.getUser().setPassword(login);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
