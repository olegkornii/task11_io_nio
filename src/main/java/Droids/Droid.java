package Droids;
// Testing 2

import java.io.Serializable;

/**
 * Represent droid
 */
public class Droid implements Serializable {
    private String name;
    private int health;
    private int attack;

    public Droid(int health, int attack, String name) {
        this.health = health;
        this.attack = attack;
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public int getAttack() {
        return attack;
    }

    /**
     * Test line
     *
     * @return
     */

    public String getName() {
        return name;
    }

    //Test of comment reader
    @Override
    public String toString() {
        return "---------------------------------\n"
                + getName() + "\n" + "Health = " + getHealth()
                + "\nAttack = " + getAttack()
                + "\n-------------------------------\n";
    }
    /*
        Hello
     */
}
