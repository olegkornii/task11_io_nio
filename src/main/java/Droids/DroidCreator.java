package Droids;

class DroidCreator {

    static Droid createRandomDroid() {
        return new Droid(randHealth(), randAttack(), randName());
    }

    private static int randHealth() {
        final int CONST = 1000;
        return (int) (Math.random() * CONST);
    }

    private static int randAttack() {
        final int CONST = 100;
        return (int) (Math.random() * CONST);
    }

    private static String randName() {
        int randNum = (int) (Math.random() * 100) + 70;
        char randChar = (char) randNum;
        return randChar + " - " + randNum;
    }
}
