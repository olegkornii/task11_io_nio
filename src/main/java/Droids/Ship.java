package Droids;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {
    List<Droid> droidList = new ArrayList<Droid>();

    Ship(int n) {
        for (int i = 0; i < n; i++) {
            droidList.add(DroidCreator.createRandomDroid());
        }
    }

    private static void save(Ship ship) {
        File file = new File("/home/oleg/home/oleg/Документи/EpamIONIOTask/src/main/resources/file.txt");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(ship);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void load() {
        File file = new File("/home/oleg/home/oleg/Документи/EpamIONIOTask/src/main/resources/file.txt");
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Ship ship = (Ship) objectInputStream.readObject();
            System.out.println(ship.droidList);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
//        Ship ship1 = new Ship(10);
//        save(ship1);
        load();
    }
}
