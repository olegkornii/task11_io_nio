package Readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReader {
    static File txt = new File("/home/oleg/home/oleg/Документи/EpamIONIOTask/src/main/resources/text.txt");
    static File javaFile = new File("/home/oleg/home/oleg/Документи/EpamIONIOTask/src/main/java/Droids/Droid.java");

    /**
     * 410207 MilliSeconds
     */
    public static void UsualReader() {

        try {
            long before = System.currentTimeMillis();
            java.io.FileReader fileReader = new java.io.FileReader(txt);
            while (fileReader.ready()) {
                System.out.print((char) fileReader.read());
            }
            //System.out.println(i);
            long after = System.currentTimeMillis();
            System.out.println(after - before);
            long curTime = after - before;
            System.out.println(curTime / 60000 + " Min, " + ((curTime % 60000) / 60) + " Sec " + ((curTime % 60000) % 60) + " Mill");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 346471
     * 340906
     * 10  bytes - 442447
     * 100 bytes - 362536
     * 500 bytes - 439894
     * 1000 bytes - 326827
     * 2000 bytes - 303940
     * 4000 bytes - 295734
     * 8000 bytes - 295683
     * 16000 bytes - 293585
     * 32000 bytes - 294827
     * 64000 bytes - 321477
     * 128000 bytes - 302154
     * 512000 bytes - 297161
     * 1024000 bytes
     */
    public static void bufferedReader() {
        long before = System.currentTimeMillis();
        try {
            java.io.FileReader fileReader = new java.io.FileReader(txt);
            BufferedReader bufferedReader = new BufferedReader(fileReader, 4000);
            while (bufferedReader.ready()) {
                System.out.print((char) bufferedReader.read());
            }
            long after = System.currentTimeMillis();
            System.out.println(after - before);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void javaReader() {

        try {
            java.io.FileReader fileReader = new java.io.FileReader(javaFile);
            while (fileReader.ready()) {
                char ch = (char) fileReader.read();
//                char nextCh = (char) fileReader.read();
                if (ch == '/' && (char) fileReader.read() == '/') {
                    System.out.print("//");
                    while (true) {
                        char comment = (char) fileReader.read();
                        if (comment == '\n') break;
                        System.out.print(comment);

                    }
                    System.out.println();
                } else if (ch == '/' && (char) fileReader.read() == '*') {

                    System.out.print("/*");

                    while (true) {
                        char comment = (char) fileReader.read();

                        if (comment == '/') {
                            System.out.print(comment);
                            break;
                        }
                        System.out.print(comment);
                    }
                    System.out.println();
                }
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        javaReader();
        // bufferedReader();
    }
}
